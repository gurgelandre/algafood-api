package br.dev.andregurgel.algafood.api.controller;

import br.dev.andregurgel.algafood.api.assembler.EstadoInputDisassembler;
import br.dev.andregurgel.algafood.api.assembler.EstadoModelAssembler;
import br.dev.andregurgel.algafood.api.model.EstadoModel;
import br.dev.andregurgel.algafood.api.model.input.EstadoInput;
import br.dev.andregurgel.algafood.domain.exception.EntidadeEmUsoException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeNaoEncontradaException;
import br.dev.andregurgel.algafood.domain.model.Estado;
import br.dev.andregurgel.algafood.domain.repository.EstadoRepository;
import br.dev.andregurgel.algafood.domain.service.EstadoService;
import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

import static java.util.Objects.nonNull;

@RestController
@RequestMapping("/estados")
public class EstadoController {

    @Autowired
    private EstadoRepository estadoRepository;

    @Autowired
    private EstadoService estadoService;

    @Autowired
    private EstadoModelAssembler estadoModelAssembler;

    @Autowired
    private EstadoInputDisassembler estadoInputDisassembler;

    @GetMapping
    public List<EstadoModel> listar() {
        return estadoModelAssembler.toCollectionModel(estadoRepository.findAll());
    }

    @GetMapping("/{estadoId}")
    public EstadoModel buscar(@PathVariable Long estadoId) {
        return estadoModelAssembler.toModel(estadoService.buscar(estadoId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public EstadoModel adicionar(@RequestBody @Valid EstadoInput estadoInput) {
        return estadoModelAssembler.toModel(estadoService.salvar(estadoInputDisassembler.toDomainObject(estadoInput)));
    }

    @PutMapping("/{estadoId}")
    public EstadoModel atualizar(@PathVariable Long estadoId, @RequestBody @Valid EstadoInput estadoInput) {
        Estado estadoAtual = estadoService.buscar(estadoId);
        estadoInputDisassembler.copyToDomainObject(estadoInput, estadoAtual);
        return estadoModelAssembler.toModel(estadoRepository.save(estadoAtual));
    }

    @DeleteMapping("/{estadoId}")
    public void remover(@PathVariable Long estadoId) {
        estadoService.remover(estadoId);
    }
}
