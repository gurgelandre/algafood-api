package br.dev.andregurgel.algafood.api.exception_handler;

import lombok.Getter;

@Getter
public enum ProblemType {
    RECURSO_NAO_ENCONTRADO("/recurso_nao_encontrado", "Recurso não encontrado"),
    ENTIDADE_EM_USO("/entidade_em_uso", "Entidade em uso"),
    ERRO_NEGOCIO("/erro_negocio", "Erro de negocio"),
    MENSAGEM_INCOMPREENSIVEL("/mensagem_incompreensivel", "Mensagem incompreensível"),
    PARAMETRO_INVALIDO("/parametro_inválido", "Parametro inválido"),
    ERRO_DE_SISTEMA("/erro_de_sistema", "Erro de sistema"),
    DADOS_INVALIDOS("/dados_invalidos", "Dados inválidos");

    private String title;
    private String uri;

    ProblemType(String path, String title) {
        this.title = title;
        this.uri = "https://algafood.com.br" + path;
    }
}
