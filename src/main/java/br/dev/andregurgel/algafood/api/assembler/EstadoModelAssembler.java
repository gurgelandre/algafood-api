package br.dev.andregurgel.algafood.api.assembler;

import br.dev.andregurgel.algafood.api.model.EstadoModel;
import br.dev.andregurgel.algafood.domain.model.Estado;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class EstadoModelAssembler {

    @Autowired
    private ModelMapper modelMapper;

    public EstadoModel toModel(Estado estado) {
        return modelMapper.map(estado, EstadoModel.class);
    }

    public List<EstadoModel> toCollectionModel(List<Estado> estadoList) {
        return estadoList.stream().map(this::toModel).collect(Collectors.toList());
    }
}
