package br.dev.andregurgel.algafood.api.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CozinhaModel {
    private Long id;
    private String nome;
}
