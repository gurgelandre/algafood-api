package br.dev.andregurgel.algafood.api.assembler;

import br.dev.andregurgel.algafood.api.model.CozinhaModel;
import br.dev.andregurgel.algafood.domain.model.Cozinha;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CozinhaModelAssembler {

    @Autowired
    private ModelMapper modelMapper;

    public CozinhaModel toModel(Cozinha cozinha) {
        return modelMapper.map(cozinha, CozinhaModel.class);
    }

    public List<CozinhaModel> toCollectionModel(List<Cozinha> cozinhaList) {
        return cozinhaList.stream().map(this::toModel).collect(Collectors.toList());
    }
}
