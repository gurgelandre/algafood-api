package br.dev.andregurgel.algafood.api.assembler;

import br.dev.andregurgel.algafood.api.model.CidadeModel;
import br.dev.andregurgel.algafood.api.model.RestauranteModel;
import br.dev.andregurgel.algafood.domain.model.Cidade;
import br.dev.andregurgel.algafood.domain.model.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class CidadeModelAssembler {

    @Autowired
    private ModelMapper modelMapper;

    public CidadeModel toModel(Cidade cidade) {
        return modelMapper.map(cidade, CidadeModel.class);
    }

    public List<CidadeModel> toCollectionModel(List<Cidade> cidades) {
        return cidades.stream()
                .map(this::toModel)
                .collect(Collectors.toList());
    }
}
