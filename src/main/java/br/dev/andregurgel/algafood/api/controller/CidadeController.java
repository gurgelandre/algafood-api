package br.dev.andregurgel.algafood.api.controller;

import br.dev.andregurgel.algafood.api.assembler.CidadeInputDisassembler;
import br.dev.andregurgel.algafood.api.assembler.CidadeModelAssembler;
import br.dev.andregurgel.algafood.api.assembler.EstadoInputDisassembler;
import br.dev.andregurgel.algafood.api.model.CidadeModel;
import br.dev.andregurgel.algafood.api.model.input.CidadeInput;
import br.dev.andregurgel.algafood.domain.exception.EstadoNaoEncontradoException;
import br.dev.andregurgel.algafood.domain.exception.NegocioException;
import br.dev.andregurgel.algafood.domain.model.Cidade;
import br.dev.andregurgel.algafood.domain.model.Estado;
import br.dev.andregurgel.algafood.domain.repository.CidadeRepository;
import br.dev.andregurgel.algafood.domain.service.CidadeService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/cidades")
public class CidadeController {
    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private CidadeService cidadeService;

    @Autowired
    private CidadeModelAssembler cidadeModelAssembler;

    @Autowired
    private CidadeInputDisassembler cidadeInputDisassembler;

    @GetMapping
    public List<CidadeModel> listar() {
        return cidadeModelAssembler.toCollectionModel(cidadeRepository.findAll());
    }

    @GetMapping("/{cidadeId}")
    public CidadeModel buscar(@PathVariable Long cidadeId) {
        return cidadeModelAssembler.toModel(cidadeService.buscar(cidadeId));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public CidadeModel adicionar(@RequestBody @Valid CidadeInput cidadeInput) {
        try {
            return cidadeModelAssembler.toModel(cidadeService.salvar(cidadeInputDisassembler.toDomainObject(cidadeInput)));
        } catch (EstadoNaoEncontradoException e) {
            throw new NegocioException(e.getMessage(), e);
        }
    }

    @PutMapping("/{cidadeId}")
    public CidadeModel atualizar(@PathVariable Long cidadeId, @RequestBody @Valid CidadeInput cidadeInput) {
        try {
            Cidade cidadeAtual = cidadeService.buscar(cidadeId);
            cidadeInputDisassembler.copyToDomainObject(cidadeInput, cidadeAtual);
            return cidadeModelAssembler.toModel(cidadeService.salvar(cidadeAtual));
        } catch (EstadoNaoEncontradoException e) {
            throw new NegocioException(e.getMessage(), e);
        }
    }

    @DeleteMapping("/{cidadeId}")
    public void remover(@PathVariable Long cidadeId) {
        cidadeService.remover(cidadeId);
    }
}
