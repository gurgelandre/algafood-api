package br.dev.andregurgel.algafood.api.assembler;

import br.dev.andregurgel.algafood.api.model.RestauranteModel;
import br.dev.andregurgel.algafood.domain.model.Restaurante;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class RestauranteModelAssembler {

    @Autowired
    private ModelMapper modelMapper;

    public RestauranteModel toModel(Restaurante restaurante) {
        return modelMapper.map(restaurante, RestauranteModel.class);
    }

    public List<RestauranteModel> toCollectionModel(List<Restaurante> restauranteList) {
        return restauranteList.stream().map(this::toModel).collect(Collectors.toList());
    }
}
