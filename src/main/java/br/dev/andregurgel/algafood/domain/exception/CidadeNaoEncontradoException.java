package br.dev.andregurgel.algafood.domain.exception;

public class CidadeNaoEncontradoException extends EntidadeNaoEncontradaException {
    private static final long serialVersionUID = 1L;

    public CidadeNaoEncontradoException(String message) {
        super(message);
    }
    public CidadeNaoEncontradoException(Long cidadeId) {
        this(String.format("Cidade de codigo %d não existe.", cidadeId));
    }
}
