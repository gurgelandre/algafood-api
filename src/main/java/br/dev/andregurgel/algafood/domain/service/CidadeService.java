package br.dev.andregurgel.algafood.domain.service;

import br.dev.andregurgel.algafood.domain.exception.CidadeNaoEncontradoException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeEmUsoException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeNaoEncontradaException;
import br.dev.andregurgel.algafood.domain.model.Cidade;
import br.dev.andregurgel.algafood.domain.model.Estado;
import br.dev.andregurgel.algafood.domain.repository.CidadeRepository;
import br.dev.andregurgel.algafood.domain.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Objects.isNull;

@Service
public class CidadeService {
    public static final String MSG_CIDADE_EM_USO = "Cidade de codigo %d não pode ser removido, pois já está em uso.";
    @Autowired
    private CidadeRepository cidadeRepository;

    @Autowired
    private EstadoService estadoService;

    public Cidade buscar(Long cidadeId) {
        return cidadeRepository.findById(cidadeId)
                .orElseThrow(() -> new CidadeNaoEncontradoException(cidadeId));
    }

    @Transactional
    public Cidade salvar(Cidade cidade) {
        Estado estado = estadoService.buscar(cidade.getEstado().getId());
        cidade.setEstado(estado);
        return cidadeRepository.save(cidade);
    }

    @Transactional
    public void remover(Long cidadeId) {
        try {
            cidadeRepository.deleteById(cidadeId);
            cidadeRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new CidadeNaoEncontradoException(cidadeId);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format(MSG_CIDADE_EM_USO, cidadeId));
        }

    }
}
