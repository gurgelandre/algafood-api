package br.dev.andregurgel.algafood.domain.model;

public enum StatusPedido {
    CRIADO,
    CONFIRMADO,
    ENTREGUE,
    CANCELADO;
}
