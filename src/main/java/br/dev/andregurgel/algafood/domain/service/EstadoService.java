package br.dev.andregurgel.algafood.domain.service;

import br.dev.andregurgel.algafood.domain.exception.EntidadeEmUsoException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeNaoEncontradaException;
import br.dev.andregurgel.algafood.domain.exception.EstadoNaoEncontradoException;
import br.dev.andregurgel.algafood.domain.model.Estado;
import br.dev.andregurgel.algafood.domain.repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class EstadoService {
    public static final String MSG_ESTADO_EM_USO = "Estado de codigo %d não pode ser removido, pois já está em uso.";
    @Autowired
    private EstadoRepository estadoRepository;

    public Estado buscar(Long estadoId) {
        return estadoRepository.findById(estadoId)
                .orElseThrow(() -> new EstadoNaoEncontradoException(estadoId));
    }

    @Transactional
    public Estado salvar(Estado estado) {
        return estadoRepository.save(estado);
    }

    @Transactional
    public void remover(Long estadoId) {
        try {
            estadoRepository.deleteById(estadoId);
            estadoRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new EstadoNaoEncontradoException(estadoId);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format(MSG_ESTADO_EM_USO, estadoId));
        }

    }
}
