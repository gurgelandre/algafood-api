package br.dev.andregurgel.algafood.domain.service;

import br.dev.andregurgel.algafood.domain.exception.EntidadeEmUsoException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeNaoEncontradaException;
import br.dev.andregurgel.algafood.domain.exception.RestauranteNaoEncontradoException;
import br.dev.andregurgel.algafood.domain.model.Cozinha;
import br.dev.andregurgel.algafood.domain.model.Restaurante;
import br.dev.andregurgel.algafood.domain.repository.CozinhaRepository;
import br.dev.andregurgel.algafood.domain.repository.RestauranteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

import static java.util.Objects.isNull;

@Service
public class RestauranteService {
    public static final String MSG_RESTAURANTE_EM_USO = "Restaurante de codigo %d não pode ser removido, pois já está em uso.";
    @Autowired
    private RestauranteRepository restauranteRepository;

    @Autowired
    private CozinhaService cozinhaService;

    public Restaurante buscar(Long restauranteId) {
        return restauranteRepository.findById(restauranteId)
                .orElseThrow(() -> new RestauranteNaoEncontradoException(restauranteId));
    }

    @Transactional
    public Restaurante salvar(Restaurante restaurante) {
        Cozinha cozinha = cozinhaService.buscar(restaurante.getCozinha().getId());
        restaurante.setCozinha(cozinha);
        return restauranteRepository.save(restaurante);
    }

    @Transactional
    public void remover(Long restauranteId) {
        try {
            restauranteRepository.deleteById(restauranteId);
            restauranteRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new RestauranteNaoEncontradoException(restauranteId);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format(MSG_RESTAURANTE_EM_USO, restauranteId));
        }

    }
}
