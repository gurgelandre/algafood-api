package br.dev.andregurgel.algafood.domain.service;

import br.dev.andregurgel.algafood.domain.exception.CozinhaNaoEncontradaException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeEmUsoException;
import br.dev.andregurgel.algafood.domain.exception.EntidadeNaoEncontradaException;
import br.dev.andregurgel.algafood.domain.model.Cozinha;
import br.dev.andregurgel.algafood.domain.repository.CozinhaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CozinhaService {
    public static final String MSG_COZINHA_EM_USO = "Cozinha de codigo %d não pode ser removida, pois já está em uso.";
    @Autowired
    private CozinhaRepository cozinhaRepository;

    public Cozinha buscar(Long cozinhaId) {
        return cozinhaRepository.findById(cozinhaId)
                .orElseThrow(() -> new CozinhaNaoEncontradaException(cozinhaId));
    }

    @Transactional
    public Cozinha salvar(Cozinha cozinha) {
        return cozinhaRepository.save(cozinha);
    }

    @Transactional
    public void remover(Long cozinhaId) {
        try {
            cozinhaRepository.deleteById(cozinhaId);
            cozinhaRepository.flush();
        } catch (EmptyResultDataAccessException e) {
            throw new CozinhaNaoEncontradaException(cozinhaId);
        } catch (DataIntegrityViolationException e) {
            throw new EntidadeEmUsoException(String.format(MSG_COZINHA_EM_USO, cozinhaId));
        }

    }
}
