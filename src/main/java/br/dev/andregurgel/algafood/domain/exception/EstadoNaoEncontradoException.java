package br.dev.andregurgel.algafood.domain.exception;

public class EstadoNaoEncontradoException extends EntidadeNaoEncontradaException {
    private static final long serialVersionUID = 1L;

    public EstadoNaoEncontradoException(String message) {
        super(message);
    }
    public EstadoNaoEncontradoException(Long estadoId) {
        this(String.format("Estado de codigo %d não existe.", estadoId));
    }
}
